import React, { createContext } from 'react';
import App from './App';
import Store from './store/store';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
  import 'react-toastify/dist/ReactToastify.css';

interface State {
	store: Store
}

const store = new Store();

export const Context = createContext<State>({ store })

ReactDOM.render(
	<Context.Provider value={{ store }}>
		<Router>
			<App />
		</Router>
	</Context.Provider>,
	document.getElementById('root')
);
