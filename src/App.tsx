import './App.css';
import React, { FC, useContext, useState } from 'react';
  import 'react-toastify/dist/ReactToastify.css';
import { observer } from 'mobx-react-lite';
import { Context } from 'index';
import VideosComponent from 'components/videos/Videos';
import { Col, Menu, MenuProps, Row } from 'antd';
import {
  AppstoreOutlined,
  DesktopOutlined
} from '@ant-design/icons';
import { Route, Routes, useNavigate } from 'react-router-dom';
import Frames from 'components/frames/Frames';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

export const App: FC = () => {

  const items: MenuItem[] = [
    getItem('Видео', 'videos', <DesktopOutlined />),
    getItem('Сохраненные кадры', 'frames', <AppstoreOutlined />),
  ]


  const { store } = useContext(Context);
  const navigate = useNavigate()

  const [value, setValue] = useState('')
  const [currentMenuItem, setCurrentMenuItem] = useState('')

  const clickMenuItem = (e: any) => {
    console.log("click", e)
    navigate("/" + e.key)
    setCurrentMenuItem(e.key);
  }

  return (
    <>
      <Row>
        <Col span={5}>
          <div>
            <Menu
              theme={'dark'}
              onClick={clickMenuItem}
              style={{ position: 'relative', height: '800px', paddingTop: '50px' }}
              selectedKeys={[currentMenuItem]}
              mode="inline"
              items={items}
            />
          </div>
        </Col>
        <Col span={19}>
          <Row>
            <div className={'header'}>
              <h2> События</h2>
            </div>
          </Row>
          <div>
            <Routes>
              <Route path="/" element={<VideosComponent />} />
              <Route path="/videos" element={<VideosComponent />} />
              <Route path="/frames" element={<Frames edit={false}/>} />
            </Routes>
          </div>
        </Col>
      </Row>
      <ToastContainer/>
    </>
  );
}

export default observer(App);
