import { makeAutoObservable } from "mobx"
import { getTime } from "utils/getTime"


export interface Video {
  id: string
  title: string
  name: string
  url: string
  duration?: number
}


export interface Frame {
  id: string
  url: string
  time: string
  video: Video
}

export default class Store {
  title = "titleee"

  frames = [] as Frame[]

  showToframes = true
  showFrames = false

  currentTimeVideo: number = 0

  currentVideo: Video = {} as Video

  selectedFrames = [] as Frame[]
  savedFrames = [] as Frame[]

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }


  pushFrame = async (frame: Frame) => {
    console.log(this.frames)
    this.frames.push(frame)
  }

  saveSelectedFrames = () =>{
    console.log("selectedFrames", this.selectedFrames)
    this.savedFrames = [...this.savedFrames,...this.selectedFrames]
    this.selectedFrames = []
  }

  setShowToFrames = (val: boolean) => {
     this.showToframes = val
  }

  setCurrentTimeVideo = (val: number) => {
    this.currentTimeVideo = val
 }

  setShowFrames = (val: boolean) => {
    this.showFrames = val
 }

  setCurrentVideo(video: Video) {
    this.currentVideo = video
  }

  clearSelectedFrame() {
    this.selectedFrames = []
  }

  pushSelectedFrame(e: Frame) {
     this.selectedFrames.push(e)
  }

  deleteSelectedFrame(e: Frame) {
     this.selectedFrames = this.selectedFrames.filter((el) => el.id !== e.id)
  }

  setFramesByVideo(video: Video) {
    let i = 1
    console.log("bla", video)
    if (video.duration) {
      while (i < video.duration) {
        console.log(i)
        const f: Frame = {
          id: `${i}`,
          url: `/frames/${video.name}/img${i}.jpg`,
          time: getTime(i),
          video: video
        }
        this.frames.push(f)
        i++
      }
    }

  }

  clearFrames = () => {
    this.frames = []
  }


  addNewTitle(title: string) {

    this.title = title
  }
}

