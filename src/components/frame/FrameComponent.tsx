import React, { FC, useContext, useState } from 'react';

import { observer } from 'mobx-react-lite';
import { Context } from 'index';
import "./frame.scss"
import { Frame } from 'store/store';
import { Checkbox, Col, Image, Modal, Row } from 'antd';


interface Props {
  frame: Frame
  edit?: boolean
}

const FrameComponent: FC<Props> = ({ frame, edit }) => {

  const { store } = useContext(Context);

  const [isShowBigPicture, setIsShowBigPicture] = useState(false)

  const handleClickCheckbox = (e: any) => {
    console.log(">>>>>>", e.target.checked)
    if (e.target.checked === true) {
      store.pushSelectedFrame(frame)
    } else {
      store.deleteSelectedFrame(frame)
    }

  }

  const handleClickFrameImage = () => {
    console.log(true)
    setIsShowBigPicture(true)
  }

  return (
    <div className="frameCont">
      <div className="frame">
        <div
          className={'frame__image'}
          style={{ backgroundImage: `url(${frame.url})` }}
          onClick={handleClickFrameImage}
        ></div>
        <div className={'frame_footer'}>
          <Row>
            <Col span={19}>
              <div className={'frame__name'}>Кадр №{frame.id}</div>
              <div className={'frame__time'}>{frame.time}</div>
            </Col>
            {edit && <Col span={5}>
              <Checkbox onChange={handleClickCheckbox} className="frame__checkbox"></Checkbox>
            </Col>}
          </Row>
          {!edit && <Row>
            <Col>
              <b className={'frame__video-name'}>Видео: {frame.video.title}</b>
            </Col>
          </Row>}
          <Image
            width={200}
            style={{ display: 'none' }}
            src={frame.url}
            preview={{
              visible: isShowBigPicture,
              src: frame.url,
              onVisibleChange: value => {
                setIsShowBigPicture(value);
              },
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default observer(FrameComponent);
