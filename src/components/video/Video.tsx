
import React, { FC, useContext, useEffect, useRef, useState } from 'react';

import { observer } from 'mobx-react-lite';
import { Context } from 'index';
import "./video.scss"
import { Button } from 'antd';
import { Frame } from 'store/store';

const VideoComponent: FC = () => {

    const { store } = useContext(Context);

    const [showFrames, setShowFrames] = useState(false)
    const [intervalID, setIntervalID] = useState(undefined)
    const [isLoadingFrames, setIsLoadingFrames] = useState(true)
    const [showFramesButton, setShowFramesButton] = useState(false)


    
    const videoRef = useRef(null)

    const handlerPause = () => {
        store.setShowToFrames(true)
        //@ts-ignore
        store.setCurrentTimeVideo(videoRef.current.currentTime)
    }

    const handlerTimeChange = () => {
                //@ts-ignore
        console.log("change",videoRef.current.currentTime )
        //@ts-ignore
        store.setCurrentTimeVideo(videoRef.current.currentTime)
    }

    const handlerPlay = () => {
        store.setShowToFrames(false)
    }

    return (
        <div>
            <div className="videoCont">
                <div>

                    <video controls onTimeUpdate={handlerTimeChange} onPause={() => handlerPause()} onPlay={() => handlerPlay()} ref={videoRef}>
                        <source src={store.currentVideo.url}></source>
                    </video>
                </div>
            </div>
        </div>
    );
}

export default observer(VideoComponent);
