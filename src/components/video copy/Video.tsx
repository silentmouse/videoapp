//@ts-nocheck
import React, { useContext, useRef, useState } from 'react';

import { observer } from 'mobx-react-lite';
import { Context } from 'index';
import "./video.scss"
import { Button } from 'antd';
import { Frame } from 'store/store';

const VideoComponent: FC = () => {

    const { store } = useContext(Context);

    const [showFrames, setShowFrames] = useState(false)
    const [intervalID, setIntervalID] = useState(undefined)
    const [isLoadingFrames, setIsLoadingFrames] = useState(true)
    const [showFramesButton, setShowFramesButton] = useState(false)

    const videoRef = useRef(null);
    const videoRefHidden = useRef(null);
    const canvasRef = useRef(null);

    const setFrame = () => {
        const canvas = canvasRef.current
        let ctx = canvas.getContext("2d")
        ctx.drawImage(videoRefHidden.current, 0, 0, 220, 150);
        let dataURL = canvas.toDataURL();
        let frame: Frame = {
            id: `${store.frames.length + 1}`,
            url: dataURL
        }
        store.pushFrame(frame)
    }

    const setFrames = async () => {
        clearInterval(intervalID);
        setIsLoadingFrames(true);
        store.clearFrames()
        const video = videoRefHidden.current
        const realTime = videoRef.current.currentTime
        let tmpTime = realTime - 20 > 0 ? realTime - 20 : 0 
        const refreshIntervalId = setInterval(() => {
            video.currentTime = tmpTime
            setFrame()
            tmpTime++
            if (tmpTime > realTime + 20){
                clearInterval(refreshIntervalId);
                setIsLoadingFrames(false)
            }
        }, 100)
        setIntervalID(refreshIntervalId)
    }

    return (
        <div>
            <div className="videoCont">
                <div>
                    <video controls muted ref={videoRef} onPause={() => setFrames()}>
                        <source src="/videoplay.mp4"></source>
                    </video>
                    <video muted ref={videoRefHidden} style={{'visibility': 'hidden'}}>
                        <source src="/videoplay.mp4"></source>
                    </video>
                </div>
                <canvas style={{ visibility: 'hidden' }} ref={canvasRef}></canvas>

            </div>
        </div>
    );
}

export default observer(VideoComponent);
