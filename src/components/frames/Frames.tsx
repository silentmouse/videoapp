import React, { FC, useContext, useEffect, useRef, useState } from 'react';

import { observer } from 'mobx-react-lite';
import { Context } from 'index';
import "./frames.scss"
import FrameComponent from 'components/frame/FrameComponent';
import { Col, Row } from 'antd';
import { Frame } from 'store/store';


interface Props {
    edit?: boolean
}


const Frames: FC<Props> = ({ edit }) => {

    const { store } = useContext(Context);
    const framesContRef = useRef(null)
    const [frames, setFrames] = useState([] as Frame[])

    useEffect(() => {

        if (edit) {
            setFrames(store.frames)
            setTimeout(() => {
                const yposition = store.currentTimeVideo / 4 * 163
                //@ts-ignore
                framesContRef.current.scrollTo(0, yposition)
            },50)
            
        }else{
            setFrames(store.savedFrames)
        }
    }, [])

    return (
        <div className="framesCont" ref={framesContRef} style={!edit ? {'height': '600px'} : {}}>
            {!edit && <h2 style={{'margin': '20px'}}>Сохраненные кадры</h2>}
            <Row>
                {frames.map((e) => {
                    return <Col key={e.id} span={5}>
                        <FrameComponent frame={e} edit={edit}/>
                    </Col>
                })}
            </Row>

        </div>
    );
}

export default observer(Frames);
