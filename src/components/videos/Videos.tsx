import React, { FC, useContext, useState } from 'react';

import { observer } from 'mobx-react-lite';
import { Context } from 'index';
import "./videos.scss"
import { Modal, Table } from 'antd';
import VideoComponent from "../video/Video"
import { Video } from 'store/store';
import Frames from 'components/frames/Frames';
import { toast } from 'react-toastify';



const columnsGetter = (onClick: any) => ([
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id'
    },
    {
        title: 'Наименование',
        dataIndex: 'title',
        key: 'title'
    },

    {
        title: 'Дата',
        dataIndex: 'date',
        key: 'date'
    },
    {
        title: 'Действия',
        dataIndex: 'url',
        key: 'url',
        render: (_: any, record: any) => (<div className="linkOnRecord" onClick={() => onClick({ record })}>
            Воспроизвести
        </div>),
    },
])

const data = [
    {
        key: '1',
        id: "1",
        name: 'video1',
        title: "Video1",
        url: "/video1.mp4",
        date: "21-08-2022 14:08:11",
        duration: 450,
    },
    {
        key: '2',
        id: "2",
        name: 'video2',
        title: "Video2",
        url: "/video2.mp4",
        date: "21-08-2022 15:10:11",
        duration: 193
    }
]



const VideosComponent: FC = () => {

    const { store } = useContext(Context);

    const [value, setValue] = useState('')

    const [showModalVideo, setShowModalVideo] = useState(false)

    

    const clickOnRowTitle = ({ record }: any) => {
        console.log("Record >>>> ", record)
        const video: Video = {
            id: record.id,
            name: record.name,
            url: record.url,
            title: record.title,
            duration: record.duration
        }
        store.setCurrentVideo(video)

        setShowModalVideo(true)

    }

    const handleOkModal = () => {
        setShowModalVideo(false)
    }

    const handleOkModalFrames = () => {
        store.saveSelectedFrames()
        store.setShowFrames(false)
        store.clearFrames()
        toast("Кадры удачно сохранены!");
    }

    const handleCancelModalFrames = () => {
        store.setShowFrames(false)
        store.clearFrames()
    }

    const handleCancelModal = () => {
        setShowModalVideo(false)
    }

    const handleClickToFrames = () => {
        store.setFramesByVideo(store.currentVideo)
        store.setShowFrames(true)
    }

    return (
        <div className="videosCont">
            <Table dataSource={data} columns={columnsGetter(clickOnRowTitle)}>

            </Table>
            {showModalVideo && <Modal title={store.currentVideo.title} width={'70%'} visible={showModalVideo} onOk={handleOkModal} onCancel={handleCancelModal}>
                {store.showToframes && <div className={'toFrames'} onClick={handleClickToFrames}>
                    К кадрам &gt;
                </div>}
                <VideoComponent />
            </Modal>}
            {store.showFrames && <Modal title={'Формирование кадра'} width={'70%'} visible={store.showToframes} onOk={handleOkModalFrames} onCancel={handleCancelModalFrames}>
                <Frames edit={true}/>
            </Modal>
            }
            
        </div>
    );
}

export default observer(VideosComponent);
