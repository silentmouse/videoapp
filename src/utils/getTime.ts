export const getTime = (seconds: number) => {
    const date = new Date(seconds * 1000)
    return date.toTimeString().split(' ')[0]
}